<?php

namespace App\Fcm\Topic;

use Exception;
use GuzzleHttp\Client;

class Subscription{
    private $apiToken;
    private $devices = [];
    private $topicName;
    public function __construct(string $topicName, string $deviceId = ''){
        $this->topicName = $topicName;
        $this->apiToken  = env("server_key", "somedefaultvalue");
        if (!empty($deviceId)) {
            $this->addDevice($deviceId);
        }
    }
    public function addDevice($deviceId){
        if (empty($deviceId)) {
            throw new Exception('Device id is empty');
        }
        if (\is_string($deviceId)) {
            $this->devices[] = $deviceId;
        }
        if (\is_array($deviceId)) {
            $this->devices = array_merge($this->devices, $deviceId);
        }

        return $this;
    }
    public function getBody(){
        return [
            'to' => "/topics/{$this->topicName}",
            'registration_tokens' => $this->devices,
        ];
    }
    public function getGuzzleClient(){
        return new Client([
            'headers' => [
                'Authorization' => 'key='.$this->apiToken
            ]
        ]);
    }
    public function send($value){
        $url = '';
        if($value === 'unsubscribe'){
            $url = 'https://iid.googleapis.com/iid/v1:batchRemove';
        }elseif ($value === 'subscribe') {
            $url = 'https://iid.googleapis.com/iid/v1:batchAdd';
        }
        $client = $this->getGuzzleClient();
        $response = $client->post($url, [
            'json' => $this->getBody()
        ]);
        $body = json_decode($response->getBody()->getContents(), true);
        if ($body === null || json_last_error() !== JSON_ERROR_NONE) {
            throw new Exception('Failed to json decode response body: '.json_last_error_msg());
        }
        return $body;
    }
}
